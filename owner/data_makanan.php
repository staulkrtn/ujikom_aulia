<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
      <div class="page-title">
        <h1>Dashboard</h1>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">Table</a></li>
          <li class="active">Data table</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<div class="content mt-3">
  <div class="animated">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Table Data Makanan</strong>
          </div>
          <div class="card-body">
            <a href="laporan_datamak.php" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</a>
                       <br><br>
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga</th>
                  <th>Gambar</th>
                </tr>
              </thead>
              <tbody>
                <?php
                error_reporting(0);
                $no = 1;
                foreach($db->tampil_data_masakan() as $x){
                  $harga = $x['harga'];
                  $hasil = "Rp".number_format($harga,2,',','.');
                  ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $x['nama_masakan']; ?></td>
                    <td><?php echo $hasil; ?></td>
                    <td><img src="../images/<?php echo $x['gambar']; ?>" height='100' width="150"></td>
                    
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>

<?php
include "foot.php";
?>
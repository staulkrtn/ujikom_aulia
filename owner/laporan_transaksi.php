
<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="col-sm-12">
                            
                        </div>
                        <div class="card-header">
                            <strong class="card-title">Table Data Transaksi Berdasarkan Tanggal</strong>
                         </div>
                        <div class="card-body"> 
                            <form action="laporan_transaksi.php" method="post" name="postform">
                                <p align="center"><font color="red" size="3"><b>Pencarian Data Berdasarkan Periode Tanggal</b></font></p><br />
                                <table border="0">
                                    <tr>
                                        <td width="125"><b>Dari Tanggal</b></td>
                                        <td colspan="2" width="190">: <input type="date" name="tanggal_awal" size="16" />                
                                        </td>
                                        <td width="125"><b>Sampai Tanggal</b></td>
                                        <td colspan="2" width="190">: <input type="date" name="tanggal_akhir" size="16" />                
                                        </td>
                                        <td colspan="2" width="190"><input type="submit" value="Pencarian Data" name="pencarian"/></td>
                                        <td colspan="2" width="70"><input type="reset" value="Reset" /></td>
                                    </tr>
                                </table>
                            </form>
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                    <?php
                                    $no = 1;
                                    //proses jika sudah klik tombol pencarian data
                                    if(isset($_POST['pencarian'])){
                                    //menangkap nilai form
                                    $tanggal_awal=$_POST['tanggal_awal'];
                                    $tanggal_akhir=$_POST['tanggal_akhir'];
                                    if(empty($tanggal_awal) || empty($tanggal_akhir)){
                                    //jika data tanggal kosong
                                    ?>
                                    <script language="JavaScript">
                                        alert('Tanggal Awal dan Tanggal Akhir Harap di Isi!');
                                        document.location='laporan';
                                    </script>

                                    <?php
                                    }else{
                                    ?><br><i><b>Informasi : </b> Hasil pencarian data berdasarkan periode Tanggal <b><?php echo $_POST['tanggal_awal']?></b> s/d <b><?php echo $_POST['tanggal_akhir']?></b></i><br><br>
                                    <?php
                                    $query=mysqli_query($conn,"SELECT * FROM transaksi INNER JOIN user ON transaksi.id_user = user.id_user where tanggal between '$tanggal_awal' and '$tanggal_akhir' order by tanggal ASC");
                                    }
                                ?>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Id Transaksi</th>
                                        <th class="text-center">Nama User</th>
                                        <th class="text-center">Id Order</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Total Harga</th>
                                        <th class="text-center">Jumlah Uang</th>
                                        <th class="text-center">Kembalian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $total = 0;
                                        //menampilkan pencarian data
                                        while($x=mysqli_fetch_array($query)){
                                        ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $x['id_transaksi']; ?></td>
                                            <td><?php echo $x['nama_user']; ?></td>
                                            <td><?php echo $x['id_order']; ?></td>
                                            <td><?php echo $x['tanggal']; ?></td>
                                            <td>Rp. <?php echo number_format($x['total_bayar']); ?></td>
                                            <td>Rp. <?php echo number_format($x['jumlah_uang']); ?></td>
                                            <td>Rp. <?php echo number_format($x['kembalian']); ?></td>
                                        </tr>

                                    <?php 
                                    $total = $total + $x['total_bayar'];
                                    } ?>
                                </tbody>
                                    <tr>
                                        <td colspan="5"><strong><h4 style="margin-left: 550px;">Total</h4></strong></td>
                                        <td colspan="5"><strong><h4>Rp. <?php echo number_format($total);?></h4></strong></td>
                                    </tr>
                            </table>
                            <a href="laporan_tran.php?tanggal_awal=<?php echo $_POST['tanggal_awal']?>&tanggal_akhir=<?php echo $_POST['tanggal_akhir']?>" class="btn btn-danger" target="blank">Cetak</a>
        <?php
        }
        else{
            unset($_POST['pencarian']);
        }
        ?>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
<?php
include "foot.php";
?>
<?php

include ('koneksi.php');
include ('function.php');

$uemail = $_GET['email'];
$token = $_GET['token'];

$userID = UserID($uemail); 

$verifytoken = verifytoken($userID, $token);


if(isset($_POST['submit']))
{
    $new_password = $_POST['password'];
    $new_password = md5($new_password);
    $retype_password = $_POST['password'];
    $retype_password = md5($retype_password);
    
    if($new_password == $retype_password)
    {
        $update_password = mysqli_query($conn, "UPDATE user SET password = '$new_password' WHERE id_user = $userID");
        if($update_password)
        {
                mysqli_query($conn, "UPDATE recovery_keys SET valid = 0 WHERE userID = $userID AND token ='$token'");
                $msg = 'Your password has changed successfully. Please login with your new passowrd.';
                $msgclass = 'bg-success';
        }
    }else
    {
         $msg = "Password doesn't match";
         $msgclass = 'bg-danger';
    }
    
}


?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sufee Admin - HTML5 Admin Template</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>

<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                        <img class="align-content" src="images/logo.png" alt="">
                    
                </div>
                <?php if(isset($msg)) {?>
                    <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
                <?php } ?>
            <?php if($verifytoken == 1) { ?>
                <div class="login-form">
                    <form action="" method="POST">
                        <div class="form-group">
                            <input type="password" class="form-control" name="passoword" placeholder="Masukkan Password Baru">
                        </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Masukkan Kembali Password">
                        </div>
                                <div class="checkbox">
                                    <label class="pull-right">
                                <a href="index.php">Kembali</a>
                            </label>

                                </div>
                                <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30" name="submit">Sign</button>
                                
                        </form>
                    </div>
                    <?php }else {?>
                <div class="col-lg-4 col-lg-offset-4">
                    <h2>Invalid or Broken Token</h2>
                    <p>Opps! The link you have come with is maybe broken or already used. Please make sure that you copied the link correctly or request another token from <a href="login.php">here</a>.</p>
                </div>
                 <?php }?>
            </div>
        </div>
    </div>


    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


</body>

</html>
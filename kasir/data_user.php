<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Table Data Makanan</strong>
                     </div>
                    <div class="card-body">
                        <a href="laporan_pdf.php" class="btn btn-success"><i class="fa fa-print"></i>&nbsp; Print</a>
                       <br><br>
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Id User</th>
                        			<th>Username</th>
                        			<th>Nama User</th>
                        			<th>Level</th>
                        			<th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
			                      $no = 1;
			                      foreach($db->tampil_data() as $x){
			                      ?>
			                      <tr>
			                        <td><?php echo $x['id_user']; ?></td>
			                        <td><?php echo $x['username']; ?></td>
			                        <td><?php echo $x['nama_user']; ?></td>
			                        <td><?php echo $x['nama_level']; ?></td>
			                        <td><?php echo $x['email']; ?></td>
			                   </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
			
<?php
include "foot.php";
?>
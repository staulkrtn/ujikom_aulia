
<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="col-sm-12">
                            
                        </div>
                        <div class="card-header">
                            <strong class="card-title">Table Data Transaksi</strong>
                         </div>
                        <div class="card-body">
                                <?php 
                                    if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {
                                    echo '<div id="pesan" class="alert alert-success alert-dismissible fade show"  data-dismiss="alert" aria-label="Close">'.$_SESSION['pesan'].'</div>';
                                    }
                                    $_SESSION['pesan'] = '';
                                ?>
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">No Meja</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Nama User</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Status Order</th>
                                        <th class="text-center">Keterangan Pembayaran</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    error_reporting(0);
                                    $no = 1;
                                    foreach($db->tampil_pesan_keterangan() as $x){
                                    ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $x['no_meja']; ?></td>
                                        <td><?php echo $x['tanggal']; ?></td>
                                        <td><?php echo $x['nama_user']; ?></td>
                                        <td><?php echo $x['keterangan']; ?></td>
                                        <td><?php echo $x['status_order']; ?></td>
                                        <td><?php
                                        if($x['keterangan_transaksi'] == 'Y')
                                            {
                                        ?>
                                            <?php echo "Terbayar";?>
                                        <?php
                                            }else{
                                        ?>
                                            <?php echo "Belum Terbayar";?>
                                        <?php 
                                        }
                                        ?></td>
                                        <td><a href="detail_transaksi.php?id_order=<?php echo $x['id_order']; ?>"><button type="button" class="btn btn-warning">Detail</button></a>
                                            
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
<?php
include "foot.php";
?>
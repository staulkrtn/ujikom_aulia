
<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
    <br><font size="3">Data Meja Tersedia</font>
        <div class="row">
            <?php
                                    // Include / load file koneksi.php
                                    // Cek apakah terdapat data pada page URL
            $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

            $limit = 4; // Jumlah data per halamanya

                                    // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
            $limit_start = ($page - 1) * $limit;

                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
            $data=mysqli_query($conn, "SELECT * FROM meja where status_meja='kosong' LIMIT ".$limit_start.",".$limit);
            $no = $limit_start + 1; // Untuk penomoran tabel
            while($show=mysqli_fetch_array($data)){
            $id_meja = $show['id_meja'];
            $meja = $show['no_meja'];
            ?>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-layout-grid2 text-success border-success"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text"><input type="hidden" name="id_meja" value="<?php echo $show['id_meja']?>"><h4>No Meja</h4></div>
                                <div class="stat-digit"><?php echo $meja; ?></div>
                                <a href="cart_meja.php?act1=add&amp;id_meja=<?php echo $show['id_meja']; ?> &amp;ref1=entri_order.php" class="btn btn-primary btn-block"><b>Pesan</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="pagination">
               <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="pesan_meja.php?page=1">First</a></li>
                <li><a href="pesan_meja.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM meja where status_meja='kosong' ");
            ($get_jumlah = (mysqli_fetch_array($sql2)));

            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="pesan_meja.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="pesan_meja.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="pesan_meja.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
            <ol class="breadcrumb">
                </ol>
                <div class="stat-widget-one"></div>
             </div>
              <br><font size="3">Data Meja Penuh</font>
             <div class="row"><br>
           <?php
                                    // Include / load file koneksi.php
                                    // Cek apakah terdapat data pada page URL
                                    $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                                    $limit = 4; // Jumlah data per halamanya

                                    // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                                    $limit_start = ($page - 1) * $limit;

                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                                    $data=mysqli_query($conn, "SELECT * FROM meja where status_meja='penuh' LIMIT ".$limit_start.",".$limit);
                                    $no = $limit_start + 1; // Untuk penomoran tabel
                                    while($show=mysqli_fetch_array($data)){
                                    $id_meja = $show['id_meja'];
                                    $meja = $show['no_meja'];
                                    ?>
              <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-layout-grid2 text-success border-success"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text"><input type="hidden" name="id_meja" value="<?php echo $show['id_meja']?>"><h4>No Meja</h4></div>
                                <div class="stat-digit"><?php echo $meja; ?></div>
                                <a href="cart_meja.php?act1=add&amp;id_meja=<?php echo $show['id_meja']; ?> &amp;ref1=entri_order.php" class="btn btn-primary btn-block"><b>Pesan</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
               </div>
    </div>
</div>

<?php
include "foot.php";
?>

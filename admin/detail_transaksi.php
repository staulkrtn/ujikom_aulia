

<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>
	
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
            	
                <div class="col-lg-8">
                        <div class="card">
       <?php
          include '../koneksi.php';
          $data_ambil_id_transaksi = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM transaksi where id_order='$_GET[id_order]'"));
          ?>
                        <div class="card-body">
            <?php
                $a = mysqli_query($conn,"SELECT keterangan_transaksi FROM transaksi WHERE id_order='$_GET[id_order]'");
                $b = mysqli_fetch_array($a);
                if($b[0]=="N"){
             ?>
                        	<button class="btn btn-success" type="submit" data-toggle="modal"
				             data-target="#mediumModal<?php echo $data_ambil_id_transaksi['id_transaksi'];?>"><i class="fa fa-shopping-cart">Bayar</i>
				           </button>
				           <?php } ?>
				           <br><br>
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
						                <th>Nama Masakan</th>
						                <th>Harga</th>
						                <th>Quantity</th>
						                <th>Keterangan</th>
						                <th>Status</th>
						                <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <?php
					              error_reporting(0);
					              $no = 1;
					              $total=0;

					              foreach($db->detail_tampil() as $x){
					               $harga=$x['harga'];
					               $jumlah=$x['jumlah']*$x['harga'];
					               $hasil="Rp.".number_format($harga,2,',','.');
					               $jml=$x['jml'] *$harga;
					               $jumlah1="Rp.".number_format($jml,2,',','.');
					               ?>
					               <tbody>
					               	
					                <tr>
					                  <td><?php echo $no++; ?></td>
					                  <td><?php echo $x['nama_masakan']; ?></td>
					                  <td><?php echo $hasil; ?></td>
					                  <td><?php echo $x['jml']; ?></td>
					                  <td><?php echo $x['keterangan']; ?></td>
					                  <td><?php echo $x['status_detail_order']; ?></td>
					                  <td><?php echo $jumlah1;?></td>
					                </tr>
					              </tbody>
					              <?php 
					              $total += ($jml) ;
					              $total1="Rp.".number_format($total,2,',','.');
					            }
					            ?>
					            <tr>
					              <td colspan="6" align="right">Total</td>
					              <td ><?php echo $total1;?></td>
					            </tr>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                 <div class="card">
            <?php
     include "../koneksi.php";
     $query_edit = mysqli_query($conn,"SELECT * FROM oder inner join user on oder.id_user=user.id_user inner join transaksi on oder.id_order=transaksi.id_order where oder.id_order='$_GET[id_order]'");
     $x = mysqli_fetch_array($query_edit)
     ?>
							<div class="col-md-12">
					        <div class="form-group">
					            <label>No.Meja</label>
                  				<input type="text" class="form-control" placeholder="<?php echo $x['no_meja'];?>" disabled/>
					        </div>
					        <div class="form-group">
					            <label>Nama Pelanggan</label>
                  				<input type="text" class="form-control" placeholder="<?php echo $x['nama_user'];?>" disabled/>
					        </div>
			                <div class="form-group">
			                  	<label>Tanggal</label>
			                  	<input type="text" class="form-control" placeholder="<?php echo $x['tanggal'];?>" disabled/>
			                </div>
			                <div class="form-group">
				                <label>Keterangan</label>
				                <?php
				                if($x['keterangan_transaksi'] == 'Y')
				                  	{	
				                ?>
									<input class="form-control" value="<?php echo "Sudah Terbayar"; ?>" readonly>
				                <?php
				                }else{
				                ?>
									<input class="form-control" value="<?php echo "Belum Terbayar"; ?>" readonly>
				              </div>
				          </div>
				                <?php 
				                	}
				               	?>
				        </div>
                    </div>
            </div>
        </div>
        </div>
        <div class="modal fade" id="mediumModal<?php echo $data_ambil_id_transaksi['id_transaksi'];?>" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    		<div class="modal-dialog modal-lg" role="document">
                        		<div class="modal-content">
                            		<div class="modal-header">
                            			<h4 class="modal-title"> TOTAL : <?php echo $total1; ?></h4>
                                		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  		
                            		</div>
                                	<div class="modal-body row">
                                  		<div class="col-md-12">
		                                    <form method="POST" action="update_transaksi.php?id_transaksi=<?php echo $data_ambil_id_transaksi['id_transaksi'];?>">
		                                      <div class="form-group">
		                                        <label> Uang Yang Dibayarkan :</label>
		                                        <input type="hidden" class="form-control" value="<?php echo $x['no_meja']; ?>" name="id_meja"/>
		                                      </div>
		                                      <div class="form-group">
		                                        <input type="text" class="form-control" id="type1" name="jumlah_uang"/>
		                                      </div>
		                                      <div class="pull-right">
		                                        <button class="btn btn-success btn-sm" type="submit"><i class=""></i> Bayar </button>
		                                        <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true" type="button"><i class="fa fa-times"></i>
		                                        Cancel</button>
		                                      </div>
		                                    </form>
                                  		</div>
                                	</div>
                                </div>
                            </div>
                        </div>
    
    					
<?php
include "foot.php";
?>
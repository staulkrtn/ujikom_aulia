<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Table Data Makanan</strong>
                     </div>
                    <div class="card-body">
                        <a href="laporan_pdf.php" class="btn btn-success"><i class="fa fa-print"></i>&nbsp; Print</a>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mediumModal"><i class=""></i>&nbsp;+ Tambah Data</button>
                        <br><br>
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                        			<th>Username</th>
                        			<th>Password</th>
                        			<th>Nama User</th>
                        			<th>Level</th>
                        			<th>Email</th>
                        			<th>Status</th>
                        			<th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
			                      $no = 1;
			                      foreach($db->tampil_data() as $x){
			                      ?>
			                      <tr>
			                        <td><?php echo $no++; ?></td>
			                        <td><?php echo $x['username']; ?></td>
			                        <td><?php echo $x['password']; ?></td>
			                        <td><?php echo $x['nama_user']; ?></td>
			                        <td><?php echo $x['nama_level']; ?></td>
			                        <td><?php echo $x['email']; ?></td>
			                        <td>
			                          <?php
			                          if($x['status'] == 'Y')
			                          {
			                          ?>
			                          <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=not-verifed" class="btn btn-primary btn-md">
			                          Aktif
			                          </a>
			                          <?php
			                          }else{
			                          ?>
			                          <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=verifed" class="btn btn-danger btn-md">
			                          Tidak Aktif
			                          </a>
			                          <?php
			                          }
			                          ?>
			                        </td>
			                        <td>
			                       	<a href="" class="btn btn-success btn-md" data-toggle="modal" data-target="#mediumModal<?php echo $x['id_user'];?>">Edit</a>
			                       </td>
			                   </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
				          <?php
                include "../koneksi.php";
                $no=0;
                $data = "SELECT * from user";
                $bacadata = mysqli_query($conn, $data);
                while($select_result = mysqli_fetch_array($bacadata))
                {
                  ?>
            <div class="modal fade" id="mediumModal<?php echo $select_result['id_user'];?>" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">Form Edit User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                  
                  <?php
                $id = $select_result['id_user']; 
                $query_edit = mysqli_query($conn,"SELECT * FROM user WHERE id_user='$id'");
                $r = mysqli_fetch_array($query_edit);
                ?>
                <div class="modal-body">
                <form role="form"  method="POST" action="proses_hak.php?aksi=update" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="card-body card-block">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Username</label></div>
                                                    <div class="col-12 col-md-9">
                                                       <input type="hidden" name="id_user" value="<?php echo $r['id_user']?>">
                                      <input type="text" id="username" class="form-control" placeholder="Masukkan Username" name="username" value="<?php echo $r['username']?>" required/>
                                                    </div><br><br>
                                                    <div class="col col-md-3">
                                                        <label for="email-input" class=" form-control-label">Password</label></div>
                                                    <div class="col-12 col-md-9">
                                                         <input type="password" name="password" id="password" class="form-control" placeholder="">
                                                            <input type="hidden" name="password_lama" placeholder="" value="<?php echo $r['password'] ?>" required/>
                                                    </div><br><br>
                                                        <div class="col col-md-3">
                                                        <label for="email-input" class=" form-control-label">Email</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="email" class="form-control" name="email" value="<?php echo $r['email']?>" id="email" required /><br>
                                                    </div>
                                                        <div class="col col-md-3">
                                                        <label for="email-input" class=" form-control-label">Nama User</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" class="form-control" name="nama_user" value="<?php echo $r['nama_user']?>" id="nama_user" required /><br>
                                                    </div>
                                                        <div class="col col-md-3"><label for="id_level" class=" form-control-label">Nama Level</label></div>
                                                            <div class="col-12 col-md-9">
                                                                <select name="id_level" class="form-control">Pilih Level
                                                                    <?php     
                                                                    include"../koneksi.php";
                                                                      $select=mysqli_query($conn, "SELECT * FROM level");
                                                                      while($show=mysqli_fetch_array($select)){
                                                                    ?>
                                                                      <option value="<?=$show['id_level'];?>" <?=$r['id_level']==$show['id_level']?'selected':null?>><?=$show['nama_level'];?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                                          </div><!-- /.box-body -->
                                                      </div>
                    </form>
                                            </div>
                            
                        </div>
                    </div>
                </div>
                <?php } ?>
				<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mediumModalLabel">Form Tambah User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            	 <form role="form"  method="POST" action="proses_hak.php?aksi=input_user" enctype="multipart/form-data" class="form-horizontal form-material"">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body card-block">
                                            <div class="form-group">
                                                	<div class="col col-md-3">
                                                		<label for="text-input" class=" form-control-label">Username</label></div>
                                                 	<div class="col-12 col-md-9">
                                                 		<input type="text" name="username" id="username" placeholder="Username" class="form-control" required /><br>
                                                 	</div>
                                                	</div>
                                                    	<div class="col col-md-3">
                                                		<label for="email-input" class=" form-control-label">Email</label></div>
                                                    <div class="col-12 col-md-9">
                                                    	<input type="email" class="form-control" name="email" id="email" placeholder="Email" required /><br>
                                                    </div>
                                                    <div class="col col-md-3">
                                                		<label for="email-input" class=" form-control-label">Password</label></div>
                                                    <div class="col-12 col-md-9">
                                                    	<input type="password" class="form-control" name="password" id="password" placeholder="Password" required /><br>
                                                    </div>
                                                    	<div class="col col-md-3">
                                                		<label for="email-input" class=" form-control-label">Nama User</label></div>
                                                    <div class="col-12 col-md-9">
                                                    	<input type="text" class="form-control" name="nama_user" id="nama_user" placeholder="Nama"  required /><br>
                                                    </div>
                                                    	<div class="col col-md-3"><label for="id_level" class=" form-control-label">Nama Level</label></div>
                                                          	<div class="col-12 col-md-9">
                                                                <select name="id_level" class="form-control">
                                                                    <option>Pilih Level</option>
                                                                	<?php
                                                                	include "../koneksi.php";
                                                                	$select = mysqli_query($conn, "SELECT * FROM level");
                                                                	while($data = mysqli_fetch_array($select)){
                                                                	?>
                                                                    <option value="<?php echo $data['id_level'];?>"><?php echo $data['nama_level'];?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
              					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            </div>
                                 </div>
                             </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
<?php
include "foot.php";
?>
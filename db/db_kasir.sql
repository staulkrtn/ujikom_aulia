-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2019 at 02:39 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL,
  `id_order` int(15) NOT NULL,
  `id_masakan` int(15) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_detail_order` varchar(30) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `jumlah`, `keterangan`, `status_detail_order`) VALUES
(85, 69, 21, 1, 'X', 'Menunggu'),
(86, 70, 21, 1, 'scs', 'Menunggu'),
(87, 70, 22, 1, 'asd', 'Menunggu'),
(88, 71, 21, 1, 'df', 'Dianntar'),
(89, 72, 21, 1, 'sd', 'Dianntar'),
(90, 73, 21, 1, 'sx', 'Dianntar'),
(91, 74, 21, 1, 'gfg', 'Dianntar'),
(92, 75, 21, 1, 'sd', 'Dianntar'),
(93, 75, 22, 2, 'asd', 'Dianntar'),
(94, 76, 21, 1, 'sd', 'Dianntar'),
(95, 78, 22, 1, 'sd', 'Menunggu'),
(96, 79, 23, 1, 'sad', 'Menunggu'),
(97, 79, 34, 1, 'sad', 'Menunggu'),
(98, 80, 22, 1, 'sd', 'Menunggu'),
(99, 80, 33, 1, 'sd', 'Menunggu'),
(100, 81, 21, 1, 'dfd', 'Menunggu'),
(101, 82, 24, 1, '', 'Menunggu'),
(102, 82, 34, 1, '', 'Menunggu'),
(103, 83, 21, 1, 'dbs', 'Y'),
(104, 84, 21, 1, 'ds', 'Menunggu'),
(105, 84, 33, 1, 'd', 'Menunggu'),
(106, 85, 24, 1, '', 'Menunggu'),
(107, 85, 23, 2, '', 'Menunggu'),
(108, 85, 33, 1, '', 'Menunggu'),
(109, 86, 23, 4, '', 'Menunggu'),
(110, 86, 28, 1, '', 'Menunggu'),
(111, 86, 24, 1, '', 'Menunggu'),
(112, 86, 34, 1, '', 'Menunggu'),
(113, 86, 30, 1, '', 'Menunggu'),
(114, 86, 27, 1, '', 'Menunggu'),
(115, 87, 21, 1, 'fczdx', 'Y'),
(116, 88, 21, 2, 'dcbz', 'Y'),
(117, 89, 23, 1, 'x', 'Menunggu'),
(118, 89, 34, 1, 's', 'Menunggu'),
(119, 90, 22, 1, '', 'Menunggu'),
(120, 91, 24, 1, '', 'Menunggu');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Makanan'),
(2, 'Minuman');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(20) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Waiter'),
(3, 'Kasir'),
(4, 'Owner'),
(5, 'Pelanggan');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL,
  `nama_masakan` varchar(30) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `gambar` varchar(500) NOT NULL,
  `harga` int(15) NOT NULL,
  `status_makanan` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Habis, Y Tersedia',
  `jenis` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `id_kategori`, `gambar`, `harga`, `status_makanan`, `jenis`) VALUES
(21, 'Bebek', 1, 'bebek-goreng.jpg', 30000, 'Y', ''),
(22, 'Ayam Bakar', 1, 'Ayam Bakar.jpg', 15000, 'Y', ''),
(24, 'Cumi Goreng Tepung', 1, 'cumi goreng tepung.jpg', 15000, 'Y', ''),
(25, 'Empal Gepuk', 1, 'empal-gepuk.jpg', 35000, 'Y', ''),
(26, 'Nasi Goreng', 1, 'nagor.jpg', 20000, 'Y', ''),
(27, 'Kepiting Saos', 1, 'kepiting saos padang.jpg', 50000, 'Y', ''),
(28, 'Kerang Saus Tiram', 1, 'kerang-hijau-saus-tiram.jpg', 45000, 'Y', '');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `id_meja` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `status_meja` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id_meja`, `no_meja`, `status_meja`) VALUES
(1, 1, 'kosong'),
(2, 2, 'penuh'),
(3, 3, 'penuh'),
(4, 4, 'penuh'),
(5, 5, 'kosong'),
(6, 6, 'penuh');

-- --------------------------------------------------------

--
-- Table structure for table `oder`
--

CREATE TABLE `oder` (
  `id_order` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_order` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oder`
--

INSERT INTO `oder` (`id_order`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_order`) VALUES
(70, 2, '2019-03-08', 8, '', 'Y'),
(71, 3, '2019-03-08', 8, '', 'Y'),
(72, 4, '2019-03-08', 8, '', 'Y'),
(73, 5, '2019-03-08', 8, '', 'Y'),
(74, 6, '2019-03-08', 8, '', 'Y'),
(75, 1, '2019-03-08', 21, '', 'Y'),
(76, 2, '2019-03-08', 21, '', 'Y'),
(77, 0, '2019-03-08', 23, '', 'Y'),
(78, 2, '2019-03-08', 23, '', 'Y'),
(79, 3, '2019-03-08', 24, '', 'Y'),
(80, 1, '2019-03-08', 21, '', 'Y'),
(81, 1, '2019-03-08', 22, '', 'Y'),
(82, 1, '2019-03-09', 21, '', 'Y'),
(83, 1, '2019-03-09', 8, '', 'N'),
(84, 2, '2019-03-09', 21, '', 'Y'),
(85, 2, '2019-03-09', 21, '', 'Y'),
(86, 5, '2019-03-09', 21, '', 'Y'),
(87, 2, '2019-03-09', 8, '', 'Y'),
(88, 5, '2019-03-09', 8, '', 'Y'),
(89, 1, '2019-03-09', 23, '', 'Y'),
(90, 0, '2019-03-29', 21, '', 'Y'),
(91, 1, '2019-03-29', 8, '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recovery_keys`
--

INSERT INTO `recovery_keys` (`rid`, `userID`, `token`, `valid`) VALUES
(1, 1, '4e95261020dbeb2f9f1ea595536a9975', 1),
(2, 1, 'fd9bbf1c5df43b94c838099742012ab5', 1),
(3, 1, '41baee1c6135aba80561d50ef473fff4', 0),
(4, 1, '28f6c086571685749d4bbba311f3a017', 0),
(5, 1, 'f603482655be4ab2a9e59e9dc278cad9', 0),
(6, 1, '015b2cfefab75e50cccb047796dff941', 1),
(7, 9, '70a69b3706dedbb691964102f57254c2', 1),
(8, 9, 'e7f7f17d565a9aa2f41a83ac7711b319', 1),
(9, 9, '8227f62588ac2a1949ff69322d497364', 1),
(10, 9, '145e274a2375cc5700bcc6680084fffc', 0),
(11, 9, '24a5c2c4a4bd64f22ea8cd2b935c1343', 1),
(12, 9, 'd0294a0b58dfae672045749f4fc7d543', 1),
(13, 9, '503242c5e92055a284f415af55f5ed5d', 1),
(14, 9, '9142fb0d6d0c22be79fb9c6e01686b96', 0),
(15, 9, 'b220612b9d697cd109d279787656ee17', 0),
(16, 9, 'fc3b5216b8853e04511758331be6eb3e', 1),
(17, 9, '372affc99619c6beca6925df421c5a93', 0),
(18, 9, '74304d829e675f7dce103bbc23950b13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(15) NOT NULL,
  `id_user` int(15) NOT NULL,
  `id_order` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(30) NOT NULL,
  `keterangan_transaksi` varchar(10) NOT NULL,
  `jumlah_uang` int(15) NOT NULL,
  `kembalian` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`, `keterangan_transaksi`, `jumlah_uang`, `kembalian`) VALUES
(56, 8, 70, '2019-03-08', 45000, 'Y', 50000, 5000),
(57, 8, 71, '2019-03-08', 30000, 'Y', 50000, 20000),
(58, 8, 72, '2019-03-08', 30000, 'Y', 50000, 20000),
(59, 8, 73, '2019-03-08', 30000, 'Y', 50000, 20000),
(60, 8, 74, '2019-03-08', 30000, 'Y', 50000, 20000),
(61, 21, 75, '2019-03-08', 60000, 'Y', 100000, 40000),
(62, 21, 76, '2019-03-08', 30000, 'Y', 50000, 20000),
(64, 8, 78, '2019-03-09', 15000, 'Y', 20000, 5000),
(65, 25, 79, '2019-03-08', 55000, 'Y', 100000, 45000),
(66, 8, 80, '2019-03-09', 30000, 'Y', 50000, 20000),
(67, 8, 81, '2019-03-09', 30000, 'Y', 50000, 20000),
(68, 8, 82, '2019-03-09', 35000, 'Y', 50000, 15000),
(70, 25, 84, '2019-03-09', 45000, 'Y', 50000, 5000),
(71, 8, 85, '2019-03-09', 100000, 'Y', 200000, 100000),
(72, 8, 86, '2019-03-09', 320000, 'Y', 500000, 180000),
(73, 0, 87, '0000-00-00', 30000, 'N', 0, 0),
(74, 8, 88, '2019-03-09', 60000, 'Y', 100000, 40000),
(75, 8, 89, '2019-03-09', 55000, 'Y', 200000, 145000),
(76, 0, 90, '0000-00-00', 15000, 'N', 0, 0),
(77, 8, 91, '2019-03-29', 15000, 'Y', 200000, 185000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `id_level` int(12) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif ,Y Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `email`, `nama_user`, `id_level`, `status`) VALUES
(8, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'pesek_hera@gmail.com', 'aulia', 1, 'Y'),
(21, 'meja1', '10705f86b703823d889c434c01419350', 'sitiauliak21@gmail.com', 'Meja 1', 5, 'Y'),
(22, 'waiter1', 'f64cff138020a2060a9817272f563b3c', 'sitiauliak21@gmail.com', 'Yuni', 2, 'Y'),
(23, 'pelanggan2', '81dc9bdb52d04dc20036dbd8313ed055', 'sitiauliak21@gmail.com', 'Meja 2', 5, 'Y'),
(24, 'meja4', '16d8fbd62375c5a77962ffd96c9275a2', 'sitiauliak21@gmail.com', 'Meja 4', 5, 'Y'),
(25, 'kasir1', 'c7911af3adbd12a035b289556d96470a', 'sitiauliak21@gmail.com', 'Kasir1', 3, 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id_detail_order`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_masakan` (`id_masakan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
  ADD PRIMARY KEY (`id_masakan`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `oder`
--
ALTER TABLE `oder`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
  MODIFY `id_masakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oder`
--
ALTER TABLE `oder`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

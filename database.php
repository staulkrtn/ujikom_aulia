
<?php 

class database{

	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "db_kasir";
	public $mysqli;

	function __construct(){
		$this->mysqli = new mysqli($this->host, $this->uname, $this->pass ,$this->db);
	}

	function tampil_data(){
		$data =$this->mysqli->query("SELECT * FROM user INNER JOIN level ON user.id_level = level.id_level GROUP BY id_user DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function input($username,$password,$email,$nama_user,$id_level,$status){
		$data =$this->mysqli->query("insert into user values('','$username','$password','$email','$nama_user','$id_level','N')");
	}	

	function hapus($id_user){
		$data = $this->mysqli->query("DELETE from user where id_user='$id_user'");
	}

	function edit($id_user){
		$data =$this->mysqli->query("SELECT * FROM user INNER JOIN level ON user.id_level = level.id_level where id_user='$id_user'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function update($id_user,$username,$password_aman,$email,$nama_user,$id_level){
		$data = $this->mysqli->query("update user set username='$username', password='$password_aman', email='$email', nama_user='$nama_user', id_level='$id_level' where id_user='$id_user'");
	}

	function tampil_data_masakan(){
		$data =$this->mysqli->query("SELECT * FROM masakan GROUP BY id_masakan DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_data_oder(){
		$data =$this->mysqli->query("SELECT * FROM oder GROUP BY id_order DESC");
		while($x = mysqli_fetch_array($data)){
			$hasil[] = $x;
		}
		return $hasil;
	}
	function tampil_data_kategori(){
		$data =$this->mysqli->query("SELECT * FROM kategori GROUP BY id_kategori DESC");
		while($x = mysqli_fetch_array($data)){
			$hasil[] = $x;
		}
		return $hasil;
	}

	function hapus_masakan($id_masakan){
		$data = $this->mysqli->query("DELETE from masakan where id_masakan='$id_masakan'");
	}
	function edit_masakan($id_masakan){
		$data = $this->mysqli->query("SELECT * from masakan where id_masakan='$id_masakan'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function update_masakan($id_masakan,$nama_masakan,$image,$harga,$status_makanan){
		$data = $this->mysqli->query("update masakan set nama_masakan='$nama_masakan', image='$image', harga='$harga', status_makanan='$status_makanan' where id_masakan='$id_masakan'");
	}
	function tampil_pesan_keterangan(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.keterangan,oder.status_order,transaksi.keterangan_transaksi, transaksi.id_transaksi, transaksi.total_bayar from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where transaksi.keterangan_transaksi='N' and oder.status_order='Y' ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function detail_tampil(){
		$data =$this->mysqli->query("SELECT *,sum(detail_order.jumlah) as jml from detail_order INNER JOIN masakan ON masakan.id_masakan= detail_order.id_masakan where detail_order.id_order='$_GET[id_order]' group by detail_order.id_masakan");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_sudah(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.keterangan,oder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where keterangan_transaksi='Y' ORDER BY id_transaksi DESC ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}	
	function tampil_data_order(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user, oder.keterangan,oder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where keterangan_transaksi='N'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}	
} 
 
?>